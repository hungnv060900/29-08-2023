package com.devcamp.bookauthorapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookauthorApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookauthorApiApplication.class, args);
	}

}
