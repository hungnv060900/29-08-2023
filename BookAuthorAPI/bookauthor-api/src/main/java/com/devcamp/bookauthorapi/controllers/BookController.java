package com.devcamp.bookauthorapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorapi.models.Book;
import com.devcamp.bookauthorapi.services.BookService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class BookController {
    @Autowired
    BookService bookService;
    @GetMapping("/books")
    public ArrayList<Book> getBook(){
        ArrayList<Book> result = bookService.getAllBook();
        return result;
    }
    
}
