package com.devcamp.bookauthorapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorapi.models.Author;
import com.devcamp.bookauthorapi.services.AuthorService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class AuthorController {
    @Autowired
    AuthorService authorService;

    @GetMapping("/author-info")
    public Author getAuthorInfo(@RequestParam String email) {
        Author result = authorService.getAuthorInfo(email);
        return result;
    }

    @GetMapping("/author-gender")
    public ArrayList<Author> getAuthorByGenders(@RequestParam char gender) {
        ArrayList<Author> result = authorService.getAuthorByGender(gender);
        return result;
    }
}
