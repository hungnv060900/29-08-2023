package com.devcamp.bookauthorapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.bookauthorapi.models.Author;
import com.devcamp.bookauthorapi.models.Book;

@Service
public class BookService {
    @Autowired 
    AuthorService authorService;
    private Book book1 = new Book("Connan" , 15.000, 10);
    private Book book2 = new Book("Connan" , 15.000, 10);
    private Book book3 = new Book("Connan" , 15.000, 10);

    public ArrayList<Book> getAllBook(){
        ArrayList<Book> books = new ArrayList<>();
        books.add( book1);
        books.add( book2);
        books.add( book3);

        return books;
    }
}
