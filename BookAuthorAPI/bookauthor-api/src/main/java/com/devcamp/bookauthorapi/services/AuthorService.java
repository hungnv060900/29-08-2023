package com.devcamp.bookauthorapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.bookauthorapi.models.Author;

@Service
public class AuthorService {
    private Author author1 = new Author("Kim Dong", "KimDong@gmail.com", 'f');
    private Author author2 = new Author("Dai Nam", "DaiNam@gmail.com", 'm');

    public ArrayList<Author> getAllAuthor() {
        ArrayList<Author> authors = new ArrayList<>();
        authors.add(author1);
        authors.add(author2);
        return authors;
    }

    public Author getAuthorInfo(String email) {
        ArrayList<Author> authors = new ArrayList<>();
        authors.add(author1);
        authors.add(author2);

        for (Author author : authors) {
            if (author.getEmail().equals(email)) {
                return author;
            }
        }

        return null;
    }

    public ArrayList<Author> getAuthorByGender(char gender) {
        ArrayList<Author> authors = new ArrayList<>();
        authors.add(author1);
        authors.add(author2);

        for (Author author : authors) {
            if (author.getGender() == gender) {
                return authors;
            }
        }

        return null;
    }

}
