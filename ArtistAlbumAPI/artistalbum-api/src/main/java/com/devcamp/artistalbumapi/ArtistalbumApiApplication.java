package com.devcamp.artistalbumapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArtistalbumApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArtistalbumApiApplication.class, args);
	}

}
