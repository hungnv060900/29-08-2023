package com.devcamp.artistalbumapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.artistalbumapi.models.Artist;
import com.devcamp.artistalbumapi.services.ArtistService;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
@CrossOrigin
@RequestMapping("/")
public class ArtistController {
    @Autowired
    ArtistService artistService;
    @GetMapping("/artists")
    public ArrayList<Artist> getAllArtists(){
        ArrayList<Artist> result = artistService.getAllArtist();
        return result;

    }
    @GetMapping("/artist-info")
    public Artist getAritstById(@RequestParam int id) {
        Artist result = artistService.getAllArtistById(id);
        return result;
    }
    
}
