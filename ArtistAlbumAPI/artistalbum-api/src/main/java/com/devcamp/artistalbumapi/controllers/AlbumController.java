package com.devcamp.artistalbumapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.artistalbumapi.models.Album;
import com.devcamp.artistalbumapi.services.AlbumService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AlbumController {
    @Autowired
    AlbumService albumService;
    @GetMapping("/album-info")
    public Album getAlbumByAlbumId(@RequestParam int id){
        Album result = albumService.getAlbum(id);
        return result;
    }
}
