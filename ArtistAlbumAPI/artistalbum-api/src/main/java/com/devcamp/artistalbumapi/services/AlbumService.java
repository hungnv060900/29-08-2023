package com.devcamp.artistalbumapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.artistalbumapi.models.Album;

@Service
public class AlbumService {
    private Album album1 = new Album(1, "dau tien");
    private Album album2 = new Album(2, "phia giua");
    private Album album3 = new Album(3, "cuoi cung");
    

    private Album album4 = new Album(4, "first");
    private Album album5 = new Album(5, "second");
    private Album album6 = new Album(6, "thrid");

    private Album album7 = new Album(7, "nhat");
    private Album album8 = new Album(8, "nhi");
    private Album album9 = new Album(9, "tam");

    public ArrayList<Album> getHNHAlbum(){
        ArrayList<Album> albums = new ArrayList<>();
        albums.add( album1);
        albums.add( album2);
        albums.add( album3);

        return albums;
    }
    public ArrayList<Album> getMTPAlbum(){
        ArrayList<Album> albums = new ArrayList<>();
        albums.add( album4);
        albums.add( album5);
        albums.add( album6);

        return albums;
    }
    public ArrayList<Album> getJackAlbum(){
        ArrayList<Album> albums = new ArrayList<>();
        albums.add( album7);
        albums.add( album8);
        albums.add( album9);

        return albums;
    }

    public Album getAlbum(int albumId) {
        ArrayList<Album> albums = new ArrayList<>();
        albums.add(album1);
        albums.add(album2);
        albums.add(album3);
        albums.add(album4);
        albums.add(album5);
        albums.add(album6);
        albums.add(album7);
        albums.add(album8);
        albums.add(album9);
        for (Album album : albums) {
            if (album.getId() == albumId) {
                return album;
            }
        }
        return null;
    }
}
