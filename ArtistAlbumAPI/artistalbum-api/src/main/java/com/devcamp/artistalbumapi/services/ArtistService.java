package com.devcamp.artistalbumapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.artistalbumapi.models.Artist;

@Service
public class ArtistService {
    
    @Autowired
    AlbumService albumService;

    private Artist artist1 = new Artist(1, "HNH");
    private Artist artist2 = new Artist(2, "MTP");
    private Artist artist3 = new Artist(3, "Jack");

    public ArrayList<Artist> getAllArtist(){
        ArrayList<Artist> artists = new ArrayList<>();
        artist1.setAlbums(albumService.getHNHAlbum());
        artist2.setAlbums(albumService.getMTPAlbum());
        artist3.setAlbums(albumService.getJackAlbum());

        artists.add( artist1);
        artists.add( artist2);
        artists.add( artist3);

        return artists;
    }

    public Artist getAllArtistById(int id){
        ArrayList<Artist> artists = new ArrayList<>();
        artist1.setAlbums(albumService.getHNHAlbum());
        artist2.setAlbums(albumService.getMTPAlbum());
        artist3.setAlbums(albumService.getJackAlbum());

        artists.add( artist1);
        artists.add( artist2);
        artists.add( artist3);

        for (Artist artist : artists) {
            if(artist.getId()==id) {
                return artist;
            }
        }

        return null;
    }

}
